import org.apache.bcel.Const;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.*;
import org.junit.Test;

import java.io.IOException;

import static org.apache.bcel.Const.ACC_PUBLIC;

public class BcelTest2 {

    public static final String INIT_METHOD_NAME = "<init>";
    public static final String BUILDER_CLASS_NAME = StringBuilder.class.getName();

    private static void invoke(ClassGen classGen) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        final BcelTest.MyClassLoader classLoader = new BcelTest.MyClassLoader(BcelTest.class.getClassLoader());
        final JavaClass javaClass = classGen.getJavaClass();
        classLoader.registerClassDefinition(javaClass.getClassName(), javaClass.getBytes());
        final Class<?> loadClass = classLoader.loadClass(classGen.getClassName());
        if (SampleClass.class.isAssignableFrom(loadClass)) {
            final Class<? extends SampleClass> aClass = (Class<? extends SampleClass>) loadClass;
            final SampleClass o = aClass.newInstance();
            System.out.println("output: '" + o.test("Micio") + "'");
        }

    }

    private static void dumpClassDefinition(ClassGen classGen) throws IOException {
        classGen.getJavaClass().dump(classGen.getClassName() + ".class");
    }

    @Test
    public void testOverriding() throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        final String className = "NewClass";
        final String methodName = "test";

        ClassGen classGen = new ClassGen(
                className,
                "SampleClass",
                "<generated>",
                Const.ACC_PUBLIC | Const.ACC_SUPER,
                null);
        ConstantPoolGen constantPool = classGen.getConstantPool();
        InstructionList instructionList = new InstructionList();
        MethodGen methodGen = new MethodGen(Const.ACC_PUBLIC,
                Type.STRING,
                new Type[]{Type.STRING},
                new String[]{"input"},
                methodName, classGen.getClassName(),
                instructionList, constantPool);



        InstructionFactory instructionFactory = new InstructionFactory(classGen);

        ObjectType builderType = new ObjectType(BUILDER_CLASS_NAME);
        // New
        instructionList.append(instructionFactory.createNew(builderType));
        instructionList.append(InstructionConst.DUP);

        // Const
        instructionList.append(new PUSH(constantPool, "Hello, "));

        // Stack
        // Reference "Factory"
        // Reference "Hello"
        // Init
        Instruction instruction = instructionFactory.createInvoke(BUILDER_CLASS_NAME, INIT_METHOD_NAME, Type.VOID, new Type[]{Type.STRING}, Const.INVOKESPECIAL);
        instructionList.append(instruction);
        // Variable
        //LocalVariableGen localVariableGen = methodGen.addLocalVariable("stringBuilder", builderType, null, null);
        //int indexStringBuilder = localVariableGen.getIndex();

        // Do Append
        //instructionList.append(new ALOAD(indexStringBuilder));
        instructionList.append(InstructionConst.ALOAD_1);
        instructionList.append(instructionFactory.createInvoke(BUILDER_CLASS_NAME, "append", builderType, new Type[]{Type.STRING}, Const.INVOKEVIRTUAL));

        instructionList.append(new PUSH(constantPool, " !"));
        instructionList.append(instructionFactory.createInvoke(BUILDER_CLASS_NAME, "append", builderType, new Type[]{Type.STRING}, Const.INVOKEVIRTUAL));
        // ToString
        //instructionList.append(new ALOAD(indexStringBuilder));
        instructionList.append(instructionFactory.createInvoke(BUILDER_CLASS_NAME, "toString", Type.STRING, new Type[]{}, Const.INVOKEVIRTUAL));
        instructionList.append(InstructionConst.ARETURN);


        methodGen.setMaxStack();
        classGen.addEmptyConstructor(ACC_PUBLIC);
        classGen.addMethod(methodGen.getMethod());
        instructionList.dispose();

        dumpClassDefinition(classGen);
        BcelTest2.invoke(classGen);
    }
}
