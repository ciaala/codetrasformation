import org.apache.bcel.Const;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.*;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.bcel.Const.ACC_PUBLIC;
import static org.apache.bcel.Const.ACC_SUPER;

/**
 * Created by cryptq on 15/06/17.
 */
public class BcelTest {
    @Test
    public void testMethodCreation() throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        ClassGen cg = new ClassGen("HelloWorld", "SampleClass",
                "<generated>", ACC_PUBLIC | ACC_SUPER, null);
        ConstantPoolGen cp = cg.getConstantPool(); // cg creates constant pool
        InstructionList il = new InstructionList();
        MethodGen mg = new MethodGen(ACC_PUBLIC, // access flags
                Type.VOID,               // return type
                new Type[]{     // argument types
                        Type.STRING},
                new String[]{"input"}, // arg names
                "test", "HelloWorld",    // method, class
                il, cp);

        InstructionFactory factory = new InstructionFactory(cg);

        ObjectType i_stream = new ObjectType("java.io.InputStream");
        ObjectType p_stream = new ObjectType("java.io.PrintStream");

        il.append(factory.createNew("java.io.BufferedReader"));
        il.append(InstructionConst.DUP); // Use predefined constant
        il.append(factory.createNew("java.io.InputStreamReader"));
        il.append(InstructionConst.DUP);
        il.append(factory.createFieldAccess("java.lang.System", "in", i_stream, Const.GETSTATIC));
        il.append(factory.createInvoke("java.io.InputStreamReader", "<init>",
                Type.VOID, new Type[]{i_stream},
                Const.INVOKESPECIAL));
        il.append(factory.createInvoke("java.io.BufferedReader", "<init>", Type.VOID,
                new Type[]{new ObjectType("java.io.Reader")},
                Const.INVOKESPECIAL));

        LocalVariableGen lg = mg.addLocalVariable("in",
                new ObjectType("java.io.BufferedReader"), null, null);
        int in = lg.getIndex();
        lg.setStart(il.append(new ASTORE(in))); // "i" valid from here

        // ---
        lg = mg.addLocalVariable("name", Type.STRING, null, null);
        int name = lg.getIndex();
        il.append(InstructionConst.ACONST_NULL);
        lg.setStart(il.append(new ASTORE(name))); // "name" valid from here

        //--
        InstructionHandle try_start =
                il.append(factory.createFieldAccess("java.lang.System", "out", p_stream, Const.GETSTATIC));

        il.append(new PUSH(cp, "Please enter your name> "));
        il.append(factory.createInvoke("java.io.PrintStream", "print", Type.VOID,
                new Type[]{Type.STRING},
                Const.INVOKEVIRTUAL));
        il.append(new ALOAD(in));
        il.append(factory.createInvoke("java.io.BufferedReader", "readLine",
                Type.STRING, Type.NO_ARGS,
                Const.INVOKEVIRTUAL));
        il.append(new ASTORE(name));

        // ---
        GOTO g = new GOTO(null);
        InstructionHandle try_end = il.append(g);

        InstructionHandle handler = il.append(InstructionConst.RETURN);
        mg.addExceptionHandler(try_start, try_end, handler, ObjectType.getInstance("java.io.IOException"));
        // ---

        InstructionHandle ih =
                il.append(factory.createFieldAccess("java.lang.System", "out", p_stream, Const.GETSTATIC));
        g.setTarget(ih);

        // ---
        il.append(factory.createNew(Type.STRINGBUFFER));
        il.append(InstructionConst.DUP);

        il.append(new PUSH(cp, "Hello, "));
        il.append(factory.createInvoke("java.lang.StringBuffer", "<init>",
                Type.VOID, new Type[]{Type.STRING},
                Const.INVOKESPECIAL));
        lg = mg.addLocalVariable("builder", Type.STRINGBUFFER, null, null);
        int builderIndex = lg.getIndex();

        il.append(new ASTORE(builderIndex));

        il.append(factory.createInvoke("java.lang.StringBuffer", "append",
                Type.STRINGBUFFER, new Type[]{Type.STRING},
                Const.INVOKEVIRTUAL));

        il.append(factory.createInvoke("java.lang.StringBuffer", "toString",
                Type.STRING, Type.NO_ARGS,
                Const.INVOKEVIRTUAL));

        il.append(factory.createInvoke("java.io.PrintStream", "println",
                Type.VOID, new Type[]{Type.STRING},
                Const.INVOKEVIRTUAL));

        il.append(new ALOAD(builderIndex));
        il.append(factory.createInvoke("java.lang.StringBuffer", "toString",
                Type.STRING, Type.NO_ARGS,
                Const.INVOKEVIRTUAL));
//        il.append(InstructionConst.ARETURN);
        il.append(InstructionConst.RETURN);
        mg.setMaxStack();
        cg.addMethod(mg.getMethod());
        il.dispose(); // Allow instruction handles to be reused
        cg.addEmptyConstructor(ACC_PUBLIC);
        try {
            cg.getJavaClass().dump(cg.getClassName() + ".class");
            if (true) {
                //final JavaClass javaClass =
                //cg.getJavaClass().getRepository().
                //final ClassLoader classLoader = cg.getClass().getClassLoader();
                MyClassLoader classLoader = new MyClassLoader(BcelTest.class.getClassLoader());
                //classLoader.
          /*  final Repository repository = cg.getJavaClass().getRepository();
            repository.storeClass(cg.getJavaClass());
            cg.getJavaClass().getBytes();
            JavaWrapper jw = new JavaWrapper(classLoader);

            //classLoader.

            ;
            */
                final JavaClass javaClass = cg.getJavaClass();
                classLoader.registerClassDefinition(javaClass.getClassName(), javaClass.getBytes());
                final Class<?> loadClass = classLoader.loadClass(cg.getClassName());
                if (SampleClass.class.isAssignableFrom(loadClass)) {
                    final Class<? extends SampleClass> aClass = (Class<? extends SampleClass>) loadClass;
                    final SampleClass o = aClass.newInstance();
                    System.out.println("Class: " + o.getClass());
                    System.out.println(o.test("zaza"));
                    //System.out.println("2 + 2 = " + calc.add(2, 2));

                }
            }
            //icl = new In
        } catch (Exception e) {
            //System.err.println(e);
            e.printStackTrace();
        }

    }

    public static class MyClassLoader extends ClassLoader {

        private final ClassLoader baseClassLoader;
        private Map<String, byte[]> streamClass = new HashMap<>();

        public MyClassLoader(final ClassLoader classLoader) {
            baseClassLoader = classLoader;
        }

        public void registerClassDefinition(final String className, final byte[] bytecode) {
            streamClass.put(className, bytecode);
        }

        @Override
        protected Class<?> findClass(String name) throws ClassNotFoundException {

            if (streamClass.containsKey(name)) {
                final byte[] bytes = streamClass.get(name);
                return defineClass(name, bytes, 0, bytes.length);
            } else {
                return super.findClass(name);
            }
        }
    }
}
