public class SupposedCode extends SampleClass {
    @Override
    public String test(final String input) {
        final StringBuilder sb = new StringBuilder("Hello, ");
        sb.append(input);
        sb.append("!");
        return sb.toString();
    }

    public String ctest(final String input) {
        final StringBuilder sb = new StringBuilder();
        return sb.toString();
    }

    public long dTest(long boundary) {
        long sum = 0;
        for (long i = 0; i <= boundary; i++) {
            if (sum % 2 == 0) {
                sum = sum + i;
            } else {
                sum = sum * i;
            }
        }
        return sum;
    }
}
